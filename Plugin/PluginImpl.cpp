#include "PluginImpl.h"
#include "Constants.h"
#include <cstring>

namespace janlenhart {
	// Body function of history writing processor thread.
	void processorThreadFunction() {
		PluginImpl *pluginImpl = PluginImpl::getInstance();

		// The last task will always be FinishTask, which will set this flag to true
		while (!pluginImpl->isShuttingDown) {
			Task *task = pluginImpl->taskBuffer->take(); // blocking call
			task->execute();
			delete task;
		}
	}

	PluginImpl* PluginImpl::instance = 0;

	PluginImpl::PluginImpl() {
		server = nullptr;
		isShuttingDown = false;
		processor = nullptr;
		taskBuffer = new BlockingBuffer<Task*>();
		outFileStream = nullptr;
	}

	PluginImpl::~PluginImpl() { } // Empty, because of singleton

	void PluginImpl::schedule(Task *task) {
		taskBuffer->put(task);
	}

	PluginImpl* PluginImpl::getInstance() {
		// No need for thread-safety, according to explanation in the task
		if (!instance) {
			instance = new PluginImpl();
		}
		return instance;
	}

	void PluginImpl::mtSrvAbout(PluginInfo *info) {
		if (info) {
			strcpy_s(info->copyright, COPYRIGHT);
			strcpy_s(info->name, PLUGIN_NAME);
			info->version = VERSION;
		} // else throw exception? API didn't specify this...
	}

	int PluginImpl::mtSrvStartup(CServerInterface *server) {
		if (!this->server && server) {
			this->server = server;
			this->outFileStream = new ofstream(HISTORY_FILE, ios_base::app);
			this->processor = new thread(processorThreadFunction);
			return 1;
		}
		return 0;
	}

	void PluginImpl::mtSrvCleanup() {
		if (!this->server) return; // instead throw exception? API didn't specify this...

		Task *finishTask = new FinishTask();
		schedule(finishTask);
		processor->join();
	}

	void PluginImpl::mtSrvHistoryTickApply(const ConSymbol *symbol, FeedTick *tick) {
		if (!this->server) return; // instead throw exception? API didn't specify this...

		Task *task = new HistoryTask(outFileStream, symbol, tick);
		schedule(task);
	}
}