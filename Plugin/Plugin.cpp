#include <ctime>
#include "Plugin.h"
#include "PluginImpl.h"

using namespace janlenhart;

void APIENTRY MtSrvAbout(PluginInfo* info) {
	PluginImpl::getInstance()->mtSrvAbout(info);
}

int APIENTRY MtSrvStartup(CServerInterface *server) {
	return PluginImpl::getInstance()->mtSrvStartup(server);
}

void APIENTRY MtSrvCleanup(void) {
	PluginImpl::getInstance()->mtSrvCleanup();
}

void APIENTRY MtSrvHistoryTickApply(const ConSymbol *symbol, FeedTick *tick) {
	PluginImpl::getInstance()->mtSrvHistoryTickApply(symbol, tick);
}
