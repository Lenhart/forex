#pragma once
#include <Windows.h>
#define _USE_32BIT_TIME_T
//+------------------------------------------------------------------+
//| Structures                                                       |
//+------------------------------------------------------------------+
//| Information about plugin                                         |
//+------------------------------------------------------------------+
struct PluginInfo
{
    char              name[128];             // plugin name
    unsigned int      version;               // plugin version
    char              copyright[128];        // plugin copyright
};
//+------------------------------------------------------------------+
//| Server access class                                              |
//+------------------------------------------------------------------+
struct CServerInterface
{
    virtual int __stdcall Version(void);
};
//+------------------------------------------------------------------+
//| Symbol config                                                  |
//+------------------------------------------------------------------+
struct ConSymbol
{
    //--- common settings
    char              symbol[12];                  // name
    char              description[64];             // description
};
//+------------------------------------------------------------------+
//| Feeder quote structure                                           |
//+------------------------------------------------------------------+
#pragma pack(push,1)
struct FeedTick
{
    char              symbol[16];                 // security
    time_t            ctm;                        // time (UNIX time)
    double            bid, ask;                   // bid/ask
};
//+------------------------------------------------------------------+
//| Plugin information                                               |
//+------------------------------------------------------------------+
__declspec(dllexport) void APIENTRY MtSrvAbout(PluginInfo* info);
__declspec(dllexport) int  APIENTRY MtSrvStartup(CServerInterface *server); // RETURN TRUE (1) if success
__declspec(dllexport) void APIENTRY MtSrvCleanup(void);
//+------------------------------------------------------------------+
//| Add new quote                                    |
//+------------------------------------------------------------------+
__declspec(dllexport) void APIENTRY MtSrvHistoryTickApply(const ConSymbol *symbol, FeedTick *tick);
//+------------------------------------------------------------------+
