#pragma once

#include <iostream>
#include <fstream>
#include "Plugin.h"

using namespace std;

namespace janlenhart {
	// Pure virtual class, represents a single task to be executed.
	class Task {
	public:
		// Implementors should put their business logic inside this method.
		virtual void execute() = 0;
	};

	// Task that writes single line of history data in provided ofstream.
	class HistoryTask : public Task {
	private:
		ConSymbol symbol;
		FeedTick tick;
		ofstream *outFileStream;		// Caller is supposed to already have opened out file stream, so there's no performance impact on opening/closing file for each line
	public:
		HistoryTask(ofstream *outFileStream, const ConSymbol *symbol, const FeedTick *tick) {
			this->outFileStream = outFileStream;
			this->symbol = *symbol;
			this->tick = *tick;
		}

		void execute() {
			// All this is just to create a nice "hour:minute:second" format of unix timestamp...
			struct tm tmBuffer;
			char timeStringBuffer[16];
			localtime_s(&tmBuffer, &tick.ctm);
			strftime(timeStringBuffer, sizeof(timeStringBuffer), "%H:%M:%S", &tmBuffer);

			// Write all data in out file stream
			*outFileStream << timeStringBuffer << "," << symbol.symbol << "," << tick.bid << ",";
			*outFileStream << tick.ask << "," << tick.ask - tick.bid << "\r\n";
		}
	};
}