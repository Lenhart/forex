#pragma once

#include <ctime>
#include "Plugin.h"
#include <thread>
#include "BlockingBuffer.h"
#include "Task.h"
#include <fstream>

using namespace std;

namespace janlenhart {
	class PluginImpl {
		friend void processorThreadFunction();		// Body of hisotry writing processor thread
		friend class FinishTask;					// Task to be added to thread when MtSrvCleanup() is called
	private:
		static PluginImpl *instance;				// Singleton instance

		CServerInterface *server;					// Cached server interface
		bool isShuttingDown;						// Flag used to tell processor thread when all work is done
													// This flag is set from inside FinishTask::execute() method

		thread *processor;							// Processor thread, writing history in file
		BlockingBuffer<Task*> *taskBuffer;			// Buffer of tick tasks to be written into history
													// Tasks will be freed by processor upon their execution

		ofstream *outFileStream;					// File stream of history

		PluginImpl();								// Private, singleton
		~PluginImpl();								// Private, singleton
		void schedule(Task *task);					// Helper method for scheduling new tasks, just in case this gets more complicated later
	public:
		static PluginImpl* getInstance();			// Singleton, if this design decision is changed don't have to implement destructor!
		
		// Implementation of MtSrvAbout().
		void mtSrvAbout(PluginInfo *info);

		// Implementation of MtSrvStartup.
		// Should return true (1) when successful.
		int mtSrvStartup(CServerInterface *server);

		// Implementation of MtSrvCleanup.
		void mtSrvCleanup(void);

		// Implementation of MtSrcHistoryTickApply.
		void mtSrvHistoryTickApply(const ConSymbol *symbol, FeedTick *tick);
	};

	// This task is supposed to be instantiated only once, on server shut down,
	// and thus it will be added to the processor's task buffer as the last task to be executed.
	// This serves dual purposes:
	// 1) Processor thread will be notified of shut down only when all previous tasks are already processed.
	// 2) If processor thread has finished all other tasks before shut down was called and thus processor is
	// blocked waiting for new task, this task will resume processor thread upon adding it to buffer.
	class FinishTask : public Task {
	public:
		void execute() {
			PluginImpl *pluginImpl = PluginImpl::getInstance();

			pluginImpl->isShuttingDown = true;
			pluginImpl->outFileStream->close();
			delete pluginImpl->outFileStream;
		}
	};
}