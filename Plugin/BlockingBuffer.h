#pragma once

#include <list>
#include <mutex>
#include <condition_variable>

using namespace std;

namespace janlenhart {
	// Thread-safe non-bounded buffer that will block calling thread when trying to read a value when buffer is empty.
	// Adding new elements will never block calling thread (the only possible lock is by mutex with consumer thread, but that should not be indefinitely).
	template <typename T>
	class BlockingBuffer {
	private:
		list<T> *data;					// storage
		mutex *myMutex;					// mutual exclusion
		condition_variable *condition;	// for blocking threads

	public:
		BlockingBuffer() {
			data = new list<T>();
			myMutex = new mutex();
			condition = new condition_variable();
		}

		~BlockingBuffer() {
			delete data;
			delete myMutex;
			delete condition;
		}

		// Put a value in the buffer.
		void put(T value) {
			unique_lock<mutex> mlock(*myMutex);
			data->push_back(value);
			mlock.unlock();
			condition->notify_one();
		}

		// Get a value from buffer.
		T take() {
			T value;
			unique_lock<mutex> mlock(*myMutex);
			while (data->size() <= 0) {
				condition->wait(mlock);
			}
			value = data->front();
			data->pop_front();
			mlock.unlock();
			return value;
		}

		// Remove all elements from the buffer.
		void clear() {
			unique_lock<mutex> mlock(*this->myMutex);
			this->data->clear();
			mlock.unlock();
		}
	};
}