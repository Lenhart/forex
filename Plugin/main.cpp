#include <iostream>
#include <ctime>
#include <Windows.h>
#include <thread>

#include "Constants.h"
#include "Plugin.h"

using namespace std;

// This is here as a dummy implementation of server
int CServerInterface::Version() {
	return -1;
}

// Do not close the window immediately
void waitUntilEnterPressed() {
	cout << "Press Enter to continue";
	cin.get();
}

// Start up the plugin and create bunch of dummy data to be logged into history
void testPlugin() {
	MtSrvStartup(new CServerInterface());

	ConSymbol cs;
	strcpy_s(cs.description, "cs_desc");
	strcpy_s(cs.symbol, "cs_symb");
	FeedTick ft;
	ft.ask = 1.2f;
	ft.bid = 1.1f;
	ft.ctm = 1510164535;
	strcpy_s(ft.symbol, "ft_symb");

	for (int i = 0; i < 100; i++) {
		for (int j = 0; j < 100; j++) {
			MtSrvHistoryTickApply(&cs, &ft);
		}
		this_thread::sleep_for(chrono::milliseconds(1));
	}

	MtSrvCleanup();
}

int main() {
	testPlugin();

	waitUntilEnterPressed();
	return 0;
}